const fs = require('fs');
if (!fs.existsSync('data.json'))
  throw new Error('read the readme (error: no data.json)')
const fsp = require('fs/promises');
const crypto = require('crypto');
const path = require('path');
let d = JSON.parse(fs.readFileSync('data.json'))

if (!Array.isArray(d))
  d = Object.values(d)

if (!fs.existsSync('out'))
  fs.mkdirSync('out')
if (!fs.existsSync('out/raw'))
  fs.mkdirSync('out/raw')
if (!fs.existsSync('out/meta'))
  fs.mkdirSync('out/meta')
if (!fs.existsSync('out/meta-failed'))
  fs.mkdirSync('out/meta-failed')

let fetching = 0;

(async () => {
  await Promise.all(d.map(async meta => {
    if (fetching > 20)
      while (fetching > 20)
        await new Promise(rs => setTimeout(rs, 100))
    fetching++;
    try {
      if (!meta.src)
        throw new Error('no src for ' + metaStr)

      let src = meta.src;
      if (src.startsWith('//'))
        src = `https:${src}`
      else if (src.startsWith('://'))
        src = `https${src}`
      else if (!src.startsWith('http'))
        src = `https://${src}`

      const response = await fetch(src);
      if (!response.ok)
        throw new Error(`Got HTTP ${response.status} (${response.statusText})`)
      const file = Buffer.from(await response.arrayBuffer());
      const hash = crypto.createHash('SHA512').update(file).digest('hex')

      const url = new URL(src);
      const urlFiletype = path.basename(url.pathname).split('.').pop()
      const filename = `${hash}.${urlFiletype}`

      const metaStr = JSON.stringify({
        ...meta,
        file: {
          relative: `../raw/${filename}`,
          filename,
        },
      }, null, 2);

      await Promise.all([
        fsp.writeFile(`out/raw/${filename}`, file),
        fsp.writeFile(`out/meta/${hash}.json`, metaStr)
      ]);
    } catch (error) {
      console.error(error)
      fs.writeFileSync('out/meta-failed/' + crypto.createHash('SHA512').update(JSON.stringify(meta)).digest('hex'), JSON.stringify({
        error,
        meta
      }))
    }
    fetching--;
  }));
})();