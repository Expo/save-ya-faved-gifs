const { exec } = require('child_process');
const fs = require('fs');
const path = require('path');

if (!fs.existsSync('out/gif'))
  fs.mkdirSync('out/gif');

const files = fs.readdirSync(__dirname + '/out/raw/');

let converting = 0;
const queued = files.filter(v => v.toLowerCase().endsWith('mp4') || v.toLowerCase().endsWith('webm'));
let remaining = queued.length;
const loop = async (queueItem) => {
  if (converting > 32)
    while (converting > 32)
      await new Promise(rs => setTimeout(rs, 10))
  converting++;
  await new Promise(resolve => {
    exec(`ffmpeg -i out/raw/${queueItem} out/gif/${queueItem.replace(/\.(mp4|webm)$/ui, '.gif')}`, {
      cwd: process.cwd()
    }, e => {
      if (e) console.error(`Error processing ${queueItem}`, e)
      else console.log('Processed', queueItem, '-', remaining, 'files left');
      resolve(void 0);
    })
  })
  converting--;
  remaining--;
}

console.log('Linking gifs');
files.filter(v => !queued.includes(v) && !fs.existsSync('out/gif/' + v)).forEach(v => fs.symlinkSync('../raw/' + v, 'out/gif/' + v))

console.log('Operating on all videos');
queued.map(loop);
