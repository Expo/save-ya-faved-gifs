# Export Discord Gifs

## Get the gifs

### 0. Clone this repo
Self-explanatory, will be used for `5.`

### 1. Set Breakpoint
Find the line containing
```js
function L(e) {
  p.DZ.updateAsync("favoriteGifs", (function(t) {
    // ...
```
in discord's webpack output. Your devtools->debugger should have this if you type ctrl+shift+f.<br/>
Variable names may be different.

### 2. Get the variable `t`
Somewhere in your devtools, it should let you access variables currently in scope. Find the variable `t`, right click it, and select something like export as global scope.

### 3. Resume Execution
Tell your devtools to resume execution.

### 4. Access it
Once you've got it, run `setTimeout(()=>navigator.clipboard.writeText(JSON.stringify(Object.values(temp0))),1000)`, replacing `temp0` with the name of the global.

Within 1 second of running, focus the main window, as you can only write to the clipboard when the window is focused. If your reaction time is shit, increase the 1000 to some higher value in milliseconds.

### 5. Write it to data.json
Write the copied files to a new file named `data.json`.

### 6. Run index.js
Run index.js with NodeJS - `node index.js`

### 7. Wait
Wait for it to complete execution - your gifs should be in the `out/raw` directory. Their metadata will be in `out/meta`.

All favourited items that couldn't be downloaded have their metadata in `out/meta-failed` alongside error information.

## Make them .gifs

Assuming you have ffmpeg installed, run `node convert.js`

